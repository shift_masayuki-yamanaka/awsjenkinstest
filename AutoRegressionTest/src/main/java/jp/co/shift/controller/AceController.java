/**
 *
 */
package jp.co.shift.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ComparisonResultBean;
import jp.co.shift.base.bean.ReproductionResultBean;
import jp.co.shift.base.bean.ResultBean;
import jp.co.shift.base.bean.SystemInputInfoBean;
import jp.co.shift.base.comparison.BeanComparison;
import jp.co.shift.base.reproduction.HttpReproduction;
import jp.co.shift.base.reproduction.SoapReproduction;
import jp.co.shift.bean.ace.ApiBean;
import jp.co.shift.constants.AceConstants;

/**
 * ACE用コントローラ
 *
 */
public class AceController extends BaseController {

	private static Logger log = LogManager.getLogger(AceController.class);

	// TODO WOS分の再現も必要

	@Override
	protected ResultBean executeRequest(SystemInputInfoBean inputInfoBean) throws Exception {
		ResultBean resultBean = null;

		// 操作内容から操作情報を取得する
		ApiBean apiBean = AceConstants.ACE_API_MAP.get(inputInfoBean.opeContents);
		if (apiBean == null) {
			log.warn("対象処理がありません。操作内容:[{}]", inputInfoBean.getOpeContents());
		}

		if (AceConstants.ACE_CONNECT_SOAP.equals(apiBean.getConnect())) {
			// SOAP通信対象の場合
			resultBean = executeSoapRequest(inputInfoBean, apiBean);

		} else {
			// REST通信対象の場合
			resultBean = executeHttpRequest(inputInfoBean, apiBean);

		}

		return resultBean;
	}

	/**
	 * SOAPリクエストを再現し、結果比較を実施する
	 *
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	private ResultBean executeSoapRequest(SystemInputInfoBean inputInfoBean, ApiBean apiBean) throws Exception {
		ResultBean resultBean = new ResultBean();
		ReproductionResultBean responseResultBean = null;
		ReproductionResultBean dbResultBean = null;

		SoapReproduction reproduction = new SoapReproduction();

		// 識別子変換
		String request = inputInfoBean.getRequest();

		responseResultBean = reproduction.execute(inputInfoBean.getSystemId(), inputInfoBean.getOpeContents(),
				inputInfoBean.getRequest());
		// 結果判定
		if (!"I00000".equals(responseResultBean.getResultCode())) {
			// TODO エラー
		}

		// 識別子管理

		// レスポンスをBeanに変換
		BaseBean srcResponseBean = xmlToBean(inputInfoBean.getResponse(), new BaseBean());
		BaseBean destResponseBean = xmlToBean(responseResultBean.getResponseInfo(), new BaseBean());

		// bean比較
		BeanComparison beanComparison = new BeanComparison();
		beanComparison.comparision(srcResponseBean, destResponseBean);

		ComparisonResultBean resuonseComparisonResultBean = beanComparison.getResultBean();
		if (resuonseComparisonResultBean.isNgResult()) {
			// TODO 比較結果NGのメッセージ追加
			responseResultBean.setResultCode("");
		}

		// DB比較
		if (apiBean.isDbComparison()) {

		}

		resultBean.setResponseResultBean(responseResultBean);
		resultBean.setDbResultBean(dbResultBean);

		return resultBean;
	}

	/**
	 * RESTリクエストを再現し、結果比較を実施する
	 *
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	private ResultBean executeHttpRequest(SystemInputInfoBean inputInfoBean, ApiBean apiBean) throws Exception {
		ResultBean resultBean = new ResultBean();
		ReproductionResultBean responseResultBean = null;
		ReproductionResultBean dbResultBean = null;

		HttpReproduction reproduction = new HttpReproduction();

		// リクエスト情報をBeanに変換する
		BaseBean requestBean = mapper.readValue(inputInfoBean.getRequest(), apiBean.getRequestBeanCls());

		// 識別子変換
		requestBean = chengeIds(requestBean, apiBean.getIdKbns(), AceConstants.ACE_ID_KBN_ITEM_MAP);

		responseResultBean = reproduction.execute(inputInfoBean.getSystemId(), inputInfoBean.getOpeContents(),
				requestBean);
		// 結果判定
		if (!"I00000".equals(responseResultBean.getResultCode())) {
			// TODO エラー
		}

		// 識別子管理

		// レスポンス情報をBeanに変換
		BaseBean srcResponseBean = mapper.readValue(inputInfoBean.getResponse(), apiBean.getRequestBeanCls());
		BaseBean destResponseBean = mapper.readValue(responseResultBean.getResponseInfo(), apiBean.getRequestBeanCls());

		// bean比較
		BeanComparison beanComparison = new BeanComparison();
		beanComparison.comparision(srcResponseBean, destResponseBean);

		ComparisonResultBean resuonseComparisonResultBean = beanComparison.getResultBean();
		if (resuonseComparisonResultBean.isNgResult()) {
			// TODO 比較結果NGのメッセージ追加
			responseResultBean.setResultCode("");
		}

		// DB比較
		if (apiBean.isDbComparison()) {

		}

		resultBean.setResponseResultBean(responseResultBean);
		resultBean.setDbResultBean(dbResultBean);

		return resultBean;
	}

	private BaseBean xmlToBean(String response, BaseBean bean) {

		return bean;
	}

}

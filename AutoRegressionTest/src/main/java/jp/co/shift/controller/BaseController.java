/**
 *
 */
package jp.co.shift.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.shift.base.Constants;
import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ReproductionResultBean;
import jp.co.shift.base.bean.ResultBean;
import jp.co.shift.base.bean.SystemInputInfoBean;
import jp.co.shift.base.db.dao.IdManagementDao;
import jp.co.shift.base.db.dto.IdManagementDto;
import jp.co.shift.base.util.AutoReqressionProperties;
import jp.co.shift.base.util.AwsS3Utils;

/**
 * コントローラの規定クラス
 *
 */
public abstract class BaseController {

	private static Logger log = LogManager.getLogger(BaseController.class);

	// JSON変換用オブジェクト
	public static ObjectMapper mapper = new ObjectMapper();

	// レポートテンプレート情報
	private static String reportTemplate = null;

	/**
	 * 処理を実行する
	 *
	 * @param systemInputInfo システムインプット情報
	 */
	public void execute(String systemInputInfo) {

		try {
			// ログ分割
			SystemInputInfoBean bean = createSystemInputInfoBean(systemInputInfo);

			long start = System.currentTimeMillis();

			// リクエスト実行
			ResultBean resultBean = executeRequest(bean);

			long end = System.currentTimeMillis();

			// レポート出力
			createReport(bean.getSystemId(), bean.getOpeContents(), end - start, resultBean.getResponseResultBean(),
					resultBean.getDbResultBean());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Json形式のSystemからのインプット情報からBeanを作成する
	 *
	 * @param systemInputInfo システムインプット情報
	 * @return システムインプット情報Bean
	 */
	private SystemInputInfoBean createSystemInputInfoBean(String systemInputInfo) {
		SystemInputInfoBean bean = null;
		if (systemInputInfo == null) {
			log.error("パラメータ未存在");
		}

		try {
			bean = mapper.readValue(systemInputInfo, SystemInputInfoBean.class);
		} catch (JsonParseException e) {
			log.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return bean;
	}

	/**
	 * リクエスト再現を実施し、レスポンス比較結果を返却する
	 *
	 * @param bean システムインプット情報Bean
	 * @return レスポンス比較結果
	 */
	protected abstract ResultBean executeRequest(SystemInputInfoBean bean) throws Exception;

	/**
	 * 識別子変換
	 *
	 * @param requestBean
	 * @param idKbns
	 * @param idKbnItemMap
	 * @return
	 */
	protected BaseBean chengeIds(BaseBean requestBean, List<String> idKbns, Map<String, List<String>> idKbnItemMap) {

		try {
			if (idKbns != null) {
				for (String idKbn : idKbns) {
					log.debug("idKbn:[{}]", idKbn);
					List<String> idItems = idKbnItemMap.get(idKbn);
					if (idItems == null || idItems.isEmpty()) {
						continue;
					}

					Field[] requestFields = requestBean.getClass().getDeclaredFields();
					for (Field requestField : requestFields) {
						if (requestField.getType().equals(List.class)) {
							// TODO：内部の型判定方法調査
						}

						if (idItems.contains(requestField.getName())) {
							// 項目名が一致した場合、識別子管理テーブルから識別子を取得し変更する
							requestField.setAccessible(true);
							String srcValue = (requestField.get(requestBean)).toString();
							String destValue = getDestIdValue(idKbn, srcValue);
							log.debug("srcValue:[{}] destValue:[{}]", srcValue, destValue);

							if (destValue != null) {
								requestField.set(requestBean, destValue);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return requestBean;
	}

	protected void insertIds(BaseBean srcResponse, BaseBean destResponse, List<String> idKbns,
			Map<String, List<String>> idKbnItemMap) {
		Field[] srcFields = srcResponse.getClass().getDeclaredFields();
		Field[] destFields = destResponse.getClass().getDeclaredFields();

		// TODO：識別子登録はDBの結果を受けて実施したい・・・
	}

	private String getDestIdValue(String idKbn, String srcValue) {
		IdManagementDao dao = new IdManagementDao();
		IdManagementDto dto = dao.selectDestIdBySrcId(idKbn, srcValue);

		if (dto == null) {
			return null;
		}
		return dto.getDestId();
	}

	private void insert(String idKbn, String srcId, String destId) {
		IdManagementDto dto = new IdManagementDto();
		dto.setIdKbn(idKbn);
		dto.setSrcId(srcId);
		dto.setDestId(destId);

		IdManagementDao dao = new IdManagementDao();
		dao.insert(dto);
	}

	/**
	 * Junit形式のXMLレポートを出力する
	 *
	 * @param systemId システムID
	 * @param opeContents 操作内容
	 * @param time 処理時間
	 * @param responceResultBean レスポンス比較結果
	 * @param dbComparisonResultBean DB比較結果
	 */
	private void createReport(String systemId, String opeContents, long time, ReproductionResultBean responceResultBean,
			ReproductionResultBean dbComparisonResultBean) {

		AutoReqressionProperties properties = AutoReqressionProperties.getInstantce();

		String failureInfo = getFailureInfo(responceResultBean, dbComparisonResultBean);
		if (StringUtils.isEmpty(failureInfo)) {
			log.info("比較結果OK");
			return;
		}

		InputStream reportOutput = null;

		// レポートテンプレートの内容を置換する
		String reportInfo = getReportTemplate();
		reportInfo.replaceAll("{className}", opeContents);
		reportInfo.replaceAll("{time}", Long.toString(time));
		reportInfo.replaceAll("{failure}", failureInfo);

		// S3バケット名
		String bucketName = properties.getProperty("");
		// オブジェクトキー(ファイル名)
		String objectKey = getReportOutputObjectKey(systemId);

		try {
			int objectSize = reportInfo.getBytes(Constants.CHAR_SET).length;

			// S3にレポートをアップロードする
			reportOutput = new ByteArrayInputStream(reportInfo.getBytes(Constants.CHAR_SET));

			AwsS3Utils s3Utils = new AwsS3Utils();
			// TODO オブジェクトkey
			s3Utils.uploadObject(bucketName, objectKey, objectSize, reportOutput);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * レポート出力内容を取得する
	 *
	 * @param responceResultBean
	 * @param dbComparisonResultBean
	 * @return
	 */
	private String getFailureInfo(ReproductionResultBean responceResultBean,
			ReproductionResultBean dbComparisonResultBean) {
		StringBuilder allInfo = new StringBuilder();
		StringBuilder ngInfo = new StringBuilder();
		String failureInfo = "";

		if (!responceResultBean.getResultCode().equals("I00000")) {
			allInfo.append("本番環境レスポンス結果：");
			allInfo.append(responceResultBean.getResultBean().getSrcAllInfo());
			allInfo.append(System.lineSeparator());
			allInfo.append("並行稼働環境レスポンス結果：");
			allInfo.append(responceResultBean.getResultBean().getDestAllInfo());
			allInfo.append(System.lineSeparator());
			allInfo.append("本番環境レスポンスNG結果：");
			allInfo.append(responceResultBean.getResultBean().getSrcNgInfo());
			allInfo.append(System.lineSeparator());
			allInfo.append("並行稼働環境レスポンスNG結果：");
			allInfo.append(responceResultBean.getResultBean().getDestNgInfo());
			allInfo.append(System.lineSeparator());
			failureInfo += allInfo.toString();
		}

		if (dbComparisonResultBean != null && !dbComparisonResultBean.getResultCode().equals("I00000")) {
			ngInfo.append("本番環境DB結果：");
			ngInfo.append(dbComparisonResultBean.getResultBean().getSrcAllInfo());
			ngInfo.append(System.lineSeparator());
			ngInfo.append("並行稼働環境DB結果：");
			ngInfo.append(dbComparisonResultBean.getResultBean().getDestAllInfo());
			ngInfo.append(System.lineSeparator());
			ngInfo.append("本番環境DB NG結果：");
			ngInfo.append(dbComparisonResultBean.getResultBean().getSrcNgInfo());
			ngInfo.append(System.lineSeparator());
			ngInfo.append("並行稼働環境DB NG結果：");
			ngInfo.append(dbComparisonResultBean.getResultBean().getDestNgInfo());
			ngInfo.append(System.lineSeparator());
			failureInfo += ngInfo.toString();
		}
		return failureInfo;
	}

	/**
	 * レポートテンプレートの情報を取得する
	 *
	 * @return
	 */
	private String getReportTemplate() {
		if (reportTemplate == null) {
			BufferedReader reader = null;
			StringBuilder builder = new StringBuilder();

			try {
				reader = new BufferedReader(
						new InputStreamReader(this.getClass().getResourceAsStream(""), Constants.CHAR_SET));
				String str = null;
				while ((str = reader.readLine()) != null) {
					builder.append(str);
					builder.append(System.lineSeparator());
				}
				reportTemplate = builder.toString();

			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally {
				IOUtils.closeQuietly(reader);
			}
		}
		return new String(reportTemplate);
	}

	/**
	 * S3にレポートを保存するパス＋ファイル名を取得する
	 *
	 * @param systemId
	 * @return
	 */
	private String getReportOutputObjectKey(String systemId) {
		AutoReqressionProperties properties = AutoReqressionProperties.getInstantce();

		return null;
	}
}
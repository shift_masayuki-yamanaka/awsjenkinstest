/**
 *
 */
package jp.co.shift.bean.ace;

import java.util.Arrays;
import java.util.List;

import jp.co.shift.base.bean.BaseBean;

/**
 * APIの実行を制御するBean
 *
 */
public class ApiBean {

	/** リクエストBeanのクラス */
	private Class<BaseBean> requestBeanCls;
	/** レスポンスBeanのクラス */
	private Class<BaseBean> responseBeanCls;

	/** 通信方法 */
	private String connect;
	/** DB比較有無 */
	private boolean isDbComparison;
	/** WOS対象 */
	private boolean isWos;
	/** 識別子区分 */
	private List<String> idKbns;

	/**
	 * コンストラクタ
	 *
	 * @param requestBeanCls
	 * @param responseBeanCls
	 * @param connect
	 * @param isDbComparison
	 * @param isWos
	 * @param idKbns
	 */
	public ApiBean(Class<BaseBean> requestBeanCls, Class<BaseBean> responseBeanCls, String connect,
			boolean isDbComparison, boolean isWos, String... idKbns) {
		this.requestBeanCls = requestBeanCls;
		this.responseBeanCls = responseBeanCls;

		this.connect = connect;
		this.isDbComparison = isDbComparison;
		this.isWos = isWos;

		if (idKbns != null) {
			this.idKbns = Arrays.asList(idKbns);
		}
	}

	/**
	 * @return requestBeanCls
	 */
	public Class<BaseBean> getRequestBeanCls() {
		return requestBeanCls;
	}

	/**
	 * @param requestBeanCls セットする requestBeanCls
	 */
	public void setRequestBeanCls(Class<BaseBean> requestBeanCls) {
		this.requestBeanCls = requestBeanCls;
	}

	/**
	 * @return responseBeanCls
	 */
	public Class<BaseBean> getResponseBeanCls() {
		return responseBeanCls;
	}

	/**
	 * @param responseBeanCls セットする responseBeanCls
	 */
	public void setResponseBeanCls(Class<BaseBean> responseBeanCls) {
		this.responseBeanCls = responseBeanCls;
	}

	/**
	 * @return connect
	 */
	public String getConnect() {
		return connect;
	}

	/**
	 * @param connect セットする connect
	 */
	public void setConnect(String connect) {
		this.connect = connect;
	}

	/**
	 * @return isDbComparison
	 */
	public boolean isDbComparison() {
		return isDbComparison;
	}

	/**
	 * @param isDbComparison セットする isDbComparison
	 */
	public void setDbComparison(boolean isDbComparison) {
		this.isDbComparison = isDbComparison;
	}

	/**
	 * @return isWos
	 */
	public boolean isWos() {
		return isWos;
	}

	/**
	 * @param isWos セットする isWos
	 */
	public void setWos(boolean isWos) {
		this.isWos = isWos;
	}

	/**
	 * @return idKbns
	 */
	public List<String> getIdKbns() {
		return idKbns;
	}

	/**
	 * @param idKbns セットする idKbns
	 */
	public void setIdKbns(List<String> idKbns) {
		this.idKbns = idKbns;
	}
}

/**
 *
 */
package jp.co.shift.base.reproduction;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.co.shift.base.Constants;
import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ReproductionResultBean;

/**
 * HTTP通信を再現する
 *
 */
public class HttpReproduction extends BaseReproduction {

	Logger log = LogManager.getLogger(HttpReproduction.class);

	@Override
	public ReproductionResultBean execute(String systemId, String opeContents, BaseBean requestBean) {
		ReproductionResultBean resultBean = new ReproductionResultBean();

		// パラメータチェック

		HttpClient client = HttpClientBuilder.create().build();

		HttpPost post = new HttpPost(getUrl(systemId, opeContents));
		try {
			post.setEntity(new UrlEncodedFormEntity(getParams(requestBean)));

			// リクエスト実行
			HttpResponse responce = client.execute(post);
			if (responce.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 通信結果OK
				// TODO:メッセージコード
				resultBean.setResultCode("");
			} else {
				// 通信結果NG
				// TODO:メッセージコード
				resultBean.setResultCode("");
			}

			String responseInfo = EntityUtils.toString(responce.getEntity(), Constants.CHAR_SET);

			resultBean.setResponseInfo(responseInfo);


		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
			resultBean.setResultCode("");
		} catch (ClientProtocolException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
			resultBean.setResultCode("");
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
			resultBean.setResultCode("");
		}

		return resultBean;
	}

	/**
	 * URLを取得する
	 *
	 * @param systemId システムID
	 * @param opeContents 操作情報
	 * @return URL
	 */
	private String getUrl(String systemId, String opeContents) {
		return null;
	}

	private List<NameValuePair> getParams(BaseBean requestBean) {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();

		try {
			Field[] fields = requestBean.getClass().getDeclaredFields();
			for (Field field : fields) {
				if (field.get(requestBean) != null) {
					pairs.add(new BasicNameValuePair(field.getName(), field.get(requestBean).toString()));
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return pairs;
	}

	@Override
	protected boolean checkSystemId(String systemId) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}
}

/**
 *
 */
package jp.co.shift.base.reproduction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ReproductionResultBean;

/**
 * RMI通信の再現を行う
 *
 */
public class RmiReproduction extends BaseReproduction {

	Logger log = LogManager.getLogger(RmiReproduction.class);

	@Override
	public ReproductionResultBean execute(String systemId, String opeContents, BaseBean requestBean) {
		ReproductionResultBean resultBean = new ReproductionResultBean();

		// パラメータチェック

		try {
			RmiRemoteDictionary object = (RmiRemoteDictionary) Naming.lookup(getRmiUrl(systemId, opeContents));

			// TODO：Dictionaryのメソッド特定
			Method method = null;
			Object response = method.invoke(object, requestBean);

		} catch (MalformedURLException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		} catch (RemoteException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		} catch (NotBoundException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		} catch (InvocationTargetException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
		}

		return resultBean;
	}

	private String getRmiUrl(String systemId, String opeContents) {
		// TODO

		return null;
	}

	@Override
	protected boolean checkSystemId(String systemId) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

}

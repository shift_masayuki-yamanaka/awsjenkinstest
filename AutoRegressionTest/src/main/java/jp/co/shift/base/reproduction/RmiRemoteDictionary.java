/**
 *
 */
package jp.co.shift.base.reproduction;

import java.rmi.Remote;

/**
 * RMI対象機能
 *
 */
public interface RmiRemoteDictionary extends Remote {

	// 実行メソッドの定義を記載
}

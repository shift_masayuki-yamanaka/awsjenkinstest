package jp.co.shift.base.db.dao;

import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * DAOの基底クラス
 *
 */
public class BaseDao {

	private Logger log = LogManager.getLogger(BaseDao.class);

	// 比較元データアクセスDS
	private DataSource srcDataSource = null;
	// 比較先データアクセスDS
	private DataSource destDataSource = null;

	/**
	 * 比較元DBコネクションを取得する
	 *
	 * @return DBコネクション
	 * @throws Exception 例外
	 */
	public Connection getSrcConnection() throws Exception {
		if (srcDataSource == null) {
			srcDataSource = createDataSource("srcDbcp.properties");
		}
		return srcDataSource.getConnection();
	}

	/**
	 * 比較先DBコネクションを取得する
	 *
	 * @return DBコネクション
	 * @throws Exception 例外
	 */
	public Connection getDestonnection() throws Exception {
		if (destDataSource == null) {
			destDataSource = createDataSource("destDbcp.properties");
		}
		return destDataSource.getConnection();
	}

	/**
	 * DataSourceを作成する
	 *
	 * @param propertyFileName プロパティファイル名
	 * @return DataSource
	 * @throws Exception
	 */
	private DataSource createDataSource(String propertyFileName) throws Exception {
		Properties properties = new Properties();
		try {
			properties.load(ClassLoader.getSystemResourceAsStream(propertyFileName));
			return BasicDataSourceFactory.createDataSource(properties);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}
}

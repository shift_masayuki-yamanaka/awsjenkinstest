/**
 *
 */
package jp.co.shift.base.comparison;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;

import jp.co.shift.base.annotation.ExclusionItem;
import jp.co.shift.base.annotation.KeyItem;
import jp.co.shift.base.annotation.SortKeyItem;
import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ComparisonResultBean;

/**
 * Bean比較実施
 *
 */
public class BeanComparison {

	private ComparisonResultBean resultBean = null;

	public BeanComparison() {
		resultBean = new ComparisonResultBean();
	}

	/**
	 * Beanを比較する
	 *
	 * @param srcBean
	 *            比較元Bean
	 * @param destBean
	 *            比較先Bean
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ComparisonResultBean comparision(BaseBean srcBean, BaseBean destBean) throws Exception {
		Field[] srcFields = srcBean.getClass().getDeclaredFields();
		Field[] destFields = destBean.getClass().getDeclaredFields();

		boolean isKeyItem = false;
		boolean isExclusionItem = false;
		boolean isNg = false;

		for (Field srcField : srcFields) {
			// 対象項目が比較対象外の場合
			if (srcField.getAnnotation(ExclusionItem.class) != null) {
				isExclusionItem = true;
			}
			// キー項目の場合
			if (srcField.getAnnotation(KeyItem.class) != null) {
				isKeyItem = true;
			}

			for (Field destField : destFields) {
				srcField.setAccessible(true);
				destField.setAccessible(true);

				if (srcField.getName().equals(destField.getName())) {
					// 除外対象でない場合
					if (!isExclusionItem) {
						if (srcField.getType().equals(List.class)) {
							// FieldがListクラスの場合
							SortKeyItem sortKeyItem = srcField.getAnnotation(SortKeyItem.class);
							List<String> sortKeyItems = new ArrayList<String>();
							sortKeyItems.add(sortKeyItem.itemName1());
							sortKeyItems.add(sortKeyItem.itemName2());
							sortKeyItems.add(sortKeyItem.itemName3());

							comparisionList(srcField.getName(), (List<BaseBean>) srcField.get(srcBean),
									(List<BaseBean>) destField.get(destBean), sortKeyItems.toArray(new String[0]));
						} else {
							if (srcField.get(srcBean) == null) {
								if (destField.get(destBean) != null) {
									isNg = true;
								}
							} else if (destField.get(destBean) == null) {
								isNg = true;
							} else {
								if (!srcField.get(srcBean).equals(destField.get(destBean))) {
									isNg = true;
								}
							}
						}
					}
					resultBean.setInfo(srcField.getName(), srcField.get(srcBean).toString(),
							destField.get(destBean).toString(), isKeyItem, isExclusionItem, isNg);
				}
			}
		}
		return resultBean;
	}

	/**
	 * ListのBeanを比較する
	 *
	 * @param srcListBean
	 *            比較元ListBean
	 * @param destListBean
	 *            比較先ListBean
	 * @return
	 * @throws Exception
	 */
	public void comparisionList(String listName, List<BaseBean> srcListBean, List<BaseBean> destListBean,
			String... sortFieldNames) throws Exception {

		resultBean.setListName(listName);

		sortList(srcListBean, sortFieldNames);
		sortList(destListBean, sortFieldNames);

		// TODO リストのサイズが違う場合にどうするか
		for (int i = 0; i < srcListBean.size(); i++) {
			comparision(srcListBean.get(i), destListBean.get(i));
		}
	}

	/**
	 * List内の中身をソートキーでソートする
	 *
	 * @param sortTargetList
	 * @param sortFieldNames
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private void sortList(List<BaseBean> sortTargetList, String[] sortFieldNames) {
		ComparatorChain comparator = new ComparatorChain();
		for (String sortFieldName : sortFieldNames) {
			if (sortFieldNames != null) {
				comparator.addComparator(new BeanComparator<Object>(sortFieldName));
			}
		}
		Collections.sort(sortTargetList, comparator);
	}

	public ComparisonResultBean getResultBean() {
		return resultBean;
	}
}

/**
 *
 */
package jp.co.shift.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Bean比較時にListの場合のソートキー項目と順番を定義する
 * <p>
 * List型の項目に設定する
 * </p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SortKeyItem {
	String itemName1();
	String itemName2();
	String itemName3();
}

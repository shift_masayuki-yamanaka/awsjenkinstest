/**
 *
 */
package jp.co.shift.base.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Message.propertiesから値を取得するクラス
 *
 */
public class MessageProperties {

	private static Logger log = LogManager.getLogger(MessageProperties.class);

	private static MessageProperties instantce = null;
	private static Properties properties = null;

	/**
	 * コンストラクタ(インスタンスを生成させない)
	 *
	 */
	private MessageProperties() {
		instantce.loadProperties();
	}

	/**
	 * インスタンスを取得する
	 *
	 * @return インスタンス
	 */
	public static MessageProperties getInstantce() {
		if (instantce == null) {
			instantce = new MessageProperties();
		}
		return instantce;
	}

	/**
	 * プロパティファイルを読み込む
	 *
	 */
	private void loadProperties() {
		properties = new Properties();
		try {
			properties.load(ClassLoader.getSystemResourceAsStream("message.properties"));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * プロパティファイルからキー該当する値を取得する
	 *
	 * @param key キー
	 * @return 値
	 */
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
}

package jp.co.shift.base.bean;

/**
 * 通信再現結果格納Bean
 *
 */
public class ReproductionResultBean {

	// 結果コード
	private String resultCode;

	// レスポンス情報
	private String responseInfo;

	// Bean比較結果
	private ComparisonResultBean resultBean;

	/**
	 * @return resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode セットする resultCode
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return responseInfo
	 */
	public String getResponseInfo() {
		return responseInfo;
	}

	/**
	 * @param responseInfo セットする responseInfo
	 */
	public void setResponseInfo(String responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * @return resultBean
	 */
	public ComparisonResultBean getResultBean() {
		return resultBean;
	}

	/**
	 * @param resultBean セットする resultBean
	 */
	public void setResultBean(ComparisonResultBean resultBean) {
		this.resultBean = resultBean;
	}

}

/**
 *
 */
package jp.co.shift.base.db.dto;

import java.util.Date;

/**
 * 識別子管理Dto
 *
 */
public class IdManagementDto extends BaseDto {

	/** 識別子区分 */
	private String idKbn;

	private String srcId;

	private String destId;

	private Date insertDate;

	private Date updateDate;

	/**
	 * @return idKbn
	 */
	public String getIdKbn() {
		return idKbn;
	}

	/**
	 * @param idKbn セットする idKbn
	 */
	public void setIdKbn(String idKbn) {
		this.idKbn = idKbn;
	}

	/**
	 * @return srcId
	 */
	public String getSrcId() {
		return srcId;
	}

	/**
	 * @param srcId セットする srcId
	 */
	public void setSrcId(String srcId) {
		this.srcId = srcId;
	}

	/**
	 * @return destId
	 */
	public String getDestId() {
		return destId;
	}

	/**
	 * @param destId セットする destId
	 */
	public void setDestId(String destId) {
		this.destId = destId;
	}

	/**
	 * @return insertDate
	 */
	public Date getInsertDate() {
		return insertDate;
	}

	/**
	 * @param insertDate セットする insertDate
	 */
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	/**
	 * @return updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate セットする updateDate
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

/**
 *
 */
package jp.co.shift.base.bean;

import java.util.Date;

/**
 * システムからのインプット情報を管理するBean
 *
 */
public class SystemInputInfoBean {

	// システムID
	public String systemId;

	// 操作日時
	public Date opeDate;

	// 操作内容
	public String opeContents;

	// 現法コード
	// TODO

	// 認証情報
	public String authInfo;

	// リクエスト情報
	public String request;

	// レスポンス情報
	public String response;

	// ファイル情報
	public String fileInfo;

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public Date getOpeDate() {
		return opeDate;
	}

	public void setOpeDate(Date opeDate) {
		this.opeDate = opeDate;
	}

	public String getOpeContents() {
		return opeContents;
	}

	public void setOpeContents(String opeContents) {
		this.opeContents = opeContents;
	}

	public String getAuthInfo() {
		return authInfo;
	}

	public void setAuthInfo(String authInfo) {
		this.authInfo = authInfo;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(String fileInfo) {
		this.fileInfo = fileInfo;
	}


}

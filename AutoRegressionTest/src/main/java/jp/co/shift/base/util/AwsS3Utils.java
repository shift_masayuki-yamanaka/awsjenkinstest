/**
 *
 */
package jp.co.shift.base.util;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

/**
 * AWSのS3用のUtilクラス
 *
 */
public class AwsS3Utils {
	private static Logger log = LogManager.getLogger(AwsS3Utils.class);

	//	private static final String ENDPOINT_URL = "https://xray.ap-northeast-1.amazonaws.com";
	//	private static final String REGION = "ap-northeast-1";
	//	private static final String ACCESS_KEY = "AKIAJOWMJZG36MV2UECA";
	//	private static final String SECRET_KEY = "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";

	/**
	 * S3にファイルをアップロードする
	 *
	 * @param bucketName
	 * @param objectKey
	 * @param objectSize
	 * @param is
	 * @throws Exception
	 */
	public void uploadObject(String bucketName, String objectKey, int objectSize, InputStream inputStream) throws Exception {

		// クライアント生成
		AmazonS3 client;
		try {
			client = getClient(bucketName);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(objectSize);

			// アップロード
			client.putObject(bucketName, objectKey, inputStream, metadata);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * S3からファイルをダウンロードする
	 *
	 * @param bucketName
	 * @param objectKey
	 * @return
	 * @throws Exception
	 */
	public BufferedInputStream downloadObject(String bucketName, String objectKey) throws Exception {

		// クライアント生成
		AmazonS3 client;
		try {
			client = getClient(bucketName);

			// ダウンロード
			S3Object s3Object = client.getObject(bucketName, objectKey);

			return new BufferedInputStream(s3Object.getObjectContent());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}

	}

	/**
	 * AWSのクライアント情報を生成する
	 *
	 * @param bucketName バケット名
	 * @return
	 * @throws Exception
	 */
	private AmazonS3 getClient(String bucketName) throws Exception {
		AutoReqressionProperties properties = AutoReqressionProperties.getInstantce();
		String accessKey = properties.getProperty(AutoReqressionProperties.KEY_S3_ACCESS_KEY);
		String secretKey = properties.getProperty(AutoReqressionProperties.KEY_S3_SECRET_KEY);
		String endpoint = properties.getProperty(AutoReqressionProperties.KEY_S3_ENDPOINT);
		String region = properties.getProperty(AutoReqressionProperties.KEY_S3_REGION);

		// 認証情報
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

		// クライアント設定
		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setProtocol(Protocol.HTTPS);
		clientConfig.setConnectionTimeout(10000);

		// エンドポイント設定
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, region);

		// クライアント生成
		AmazonS3 client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withClientConfiguration(clientConfig)
				.withEndpointConfiguration(endpointConfiguration).build();

		if (!client.doesBucketExistV2(bucketName)) {
			log.error("S3バケットがありません。baketName:[{}]", bucketName);
			throw new Exception("S3バケット[" + bucketName + "]がありません");
		}

		return client;
	}
}

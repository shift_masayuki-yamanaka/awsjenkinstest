/**
 *
 */
package jp.co.shift.base.reproduction;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ReproductionResultBean;

/**
 * FTP通信を再現する
 * <p>
 * ファイルを所定の位置に配置する
 * </p>
 *
 */
public class FtpReproduction extends BaseReproduction{

	private Logger log = LogManager.getLogger(FtpReproduction.class);

	@Override
	public ReproductionResultBean execute(String systemId, String opeContents, BaseBean requestBean) {
		ReproductionResultBean resultBean = new ReproductionResultBean();
		FileInputStream inputStream = null;
		try {
			// 所定の場所(S3)からファイルを取得する
			inputStream = getFile(systemId, opeContents, requestBean);


			// 所定の場所にファイルを配置する
			putFile(systemId, opeContents, inputStream, resultBean);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}


		return resultBean;
	}

	private FileInputStream getFile(String systemId, String opeContents, BaseBean requestBean) {
		// TODO AWS-S3からの取得が必要


		return null;
	}

	private ReproductionResultBean putFile(String systemId, String opeContents, FileInputStream inputStream, ReproductionResultBean resultBean) {

		FTPClient ftpClient = new FTPClient();

		try {
			// 接続
			ftpClient.connect(getFtpServerName(systemId, opeContents));
			if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
				// FTP接続失敗
				// TODO：メッセージコード
			}

			// ログイン
			if (!ftpClient.login(getFtpUserName(systemId, opeContents), getFtpPassword(systemId, opeContents))) {
				// ログイン失敗
				// TODO：メッセージコード
			}


			// バイナリモード設定
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			// ファイル送信
			ftpClient.storeFile("failname", inputStream);


		} catch (SocketException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
			resultBean.setResultCode("");
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			// TODO:メッセージコード
			resultBean.setResultCode("");
		} finally {
			ftpClientDisconnect(ftpClient);
		}


		return resultBean;
	}

	private String getFtpServerName(String systemId, String opeContents) {
		// TODO


		return null;
	}

	private String getFtpUserName(String systemId, String opeContents) {
		// TODO

		return null;
	}

	private String getFtpPassword(String systemId, String opeContents) {
		// TODO

		return null;
	}

	/**
	 * FTPクライアント接続を切断する
	 *
	 * @param ftpClient FTPクライアント
	 */
	private void ftpClientDisconnect(FTPClient ftpClient) {
		if (ftpClient != null) {
			if (ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (IOException e) {
				}
			}
		}
	}


	@Override
	protected boolean checkSystemId(String systemId) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

}

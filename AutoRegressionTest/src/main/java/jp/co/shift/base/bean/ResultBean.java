/**
 *
 */
package jp.co.shift.base.bean;

/**
 * 比較結果格納Bean
 *
 */
public class ResultBean {

	// レスポンス比較結果
	private ReproductionResultBean responseResultBean;
	// DB比較結果
	private ReproductionResultBean dbResultBean;

	/**
	 * @return responseResultBean
	 */
	public ReproductionResultBean getResponseResultBean() {
		return responseResultBean;
	}

	/**
	 * @param responseResultBean セットする responseResultBean
	 */
	public void setResponseResultBean(ReproductionResultBean responseResultBean) {
		this.responseResultBean = responseResultBean;
	}

	/**
	 * @return dbResultBean
	 */
	public ReproductionResultBean getDbResultBean() {
		return dbResultBean;
	}

	/**
	 * @param dbResultBean セットする dbResultBean
	 */
	public void setDbResultBean(ReproductionResultBean dbResultBean) {
		this.dbResultBean = dbResultBean;
	}
}

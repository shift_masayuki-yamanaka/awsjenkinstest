/**
 *
 */
package jp.co.shift.base;

/**
 * 定数クラス
 *
 */
public class Constants {

	// 文字コード
	public static final String CHAR_SET = "UTF-8";

	// システムID
	public static final String SYSTEM_ID_ACE = "ACE";
	public static final String SYSTEM_ID_EIP = "EIP";
	public static final String SYSTEM_ID_GEDI = "GEDI";
	public static final String SYSTEM_ID_WING =  "WING";
	public static final String SYSTEM_ID_BELL =  "BELL";
	public static final String SYSTEM_ID_MEET =  "MEET";
	public static final String SYSTEM_ID_MIND =  "MIND";
	public static final String SYSTEM_ID_MAGI =  "MAGI";

	// RabbitMQキュー名
	public static final String QUEUE_NAME_ACE = "ace.queue";
	public static final String QUEUE_NAME_EIP = "eip.queue";
	public static final String QUEUE_NAME_GEDI = "gedi.queue";
	public static final String QUEUE_NAME_WING =  "wing.queue";
	public static final String QUEUE_NAME_BELL =  "bell.queue";
	public static final String QUEUE_NAME_MEET =  "meet.queue";
	public static final String QUEUE_NAME_MIND =  "mind.queue";
	public static final String QUEUE_NAME_MAGI =  "magi.queue";


}

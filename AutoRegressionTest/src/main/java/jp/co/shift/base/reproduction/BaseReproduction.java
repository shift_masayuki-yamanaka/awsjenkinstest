/**
 *
 */
package jp.co.shift.base.reproduction;

import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.base.bean.ReproductionResultBean;

/**
 * 通信再現の規定クラス
 *
 */
public abstract class BaseReproduction {


	public abstract ReproductionResultBean execute(String systemId, String opeContents, BaseBean requestBean);

	protected abstract boolean checkSystemId(String systemId);

}
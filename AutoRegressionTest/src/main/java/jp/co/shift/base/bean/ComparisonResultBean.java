/**
 *
 */
package jp.co.shift.base.bean;

/**
 * Bean比較結果格納用
 *
 */
public class ComparisonResultBean {

	// 比較元全情報
	private StringBuilder srcAllInfo;
	// 比較先全情報
	private StringBuilder destAllInfo;
	// 比較元比較NG情報
	private StringBuilder srcNgInfo;
	// 比較先比較NG情報
	private StringBuilder destNgInfo;

	/**
	 * コンストラクタ
	 */
	public ComparisonResultBean() {
		srcAllInfo = new StringBuilder();
		destAllInfo = new StringBuilder();
		srcNgInfo = new StringBuilder();
		destNgInfo = new StringBuilder();
	}

	public String getSrcAllInfo() {
		return srcAllInfo.toString();
	}

	public String getDestAllInfo() {
		return destAllInfo.toString();
	}

	public String getSrcNgInfo() {
		return srcNgInfo.toString();
	}

	public String getDestNgInfo() {
		return destNgInfo.toString();
	}

	/**
	 * NG情報があるかどうか
	 *
	 * @return true:NGなし / false:NGあり
	 */
	public boolean isNgResult() {
		if (srcNgInfo.length() != 0 || destNgInfo.length() != 0) {
			return true;
		}
		return false;
	}

	/**
	 * 比較情報を設定する
	 *
	 * @param fieldName 項目名
	 * @param srcValue 比較元値
	 * @param destValue 比較先値
	 * @param isKeyItem キー項目かどうか
	 * @param isExclusionItem 除外項目かどうか
	 * @param isNg 比較NGかどうか
	 */
	public void setInfo(String fieldName, String srcValue, String destValue, boolean isKeyItem, boolean isExclusionItem,
			boolean isNg) {
		setInfo(fieldName, srcValue, destValue, isKeyItem, isExclusionItem);
		if (isNg) {
			setNgInfo(fieldName, srcValue, destValue, isKeyItem, isExclusionItem);
		}
	}

	/**
	 * List項目を比較する前にList名を設定する
	 *
	 * @param listName
	 */
	public void setListName(String listName) {

	}

	/**
	 * 全情報を設定する
	 *
	 * @param fieldName 項目名
	 * @param srcValue 比較元値
	 * @param destValue 比較先値
	 * @param isKeyItem キー項目かどうか
	 * @param isExclusionItem 除外項目かどうか
	 */
	private void setInfo(String fieldName, String srcValue, String destValue, boolean isKeyItem,
			boolean isExclusionItem) {
		appendStringBuilder(this.srcAllInfo, fieldName, srcValue, isKeyItem, isExclusionItem);
		appendStringBuilder(this.destAllInfo, fieldName, srcValue, isKeyItem, isExclusionItem);
	}

	/**
	 * NG情報を設定する
	 *
	 * @param fieldName 項目名
	 * @param srcValue 比較元値
	 * @param destValue 比較先値
	 * @param isKeyItem キー項目かどうか
	 * @param isExclusionItem 除外項目かどうか
	 */
	private void setNgInfo(String fieldName, String srcValue, String destValue, boolean isKeyItem,
			boolean isExclusionItem) {
		appendStringBuilder(this.srcAllInfo, fieldName, srcValue, isKeyItem, isExclusionItem);
		appendStringBuilder(this.destAllInfo, fieldName, srcValue, isKeyItem, isExclusionItem);
	}

	/**
	 * StringBuilderに値を追加する
	 *
	 * @param builder StringBuilder
	 * @param fieldName 項目名
	 * @param addValue 値
	 * @param isKeyItem キー項目かどうか
	 * @param isExclusionItem 除外項目かどうか
	 */
	private void appendStringBuilder(StringBuilder builder, String fieldName, String value, boolean isKeyItem,
			boolean isExclusionItem) {
		// TODO フォーマット調整 [キー項目][除外項目]項目名 = 値
		builder.append(value);
	}
}

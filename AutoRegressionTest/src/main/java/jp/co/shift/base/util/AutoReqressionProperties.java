/**
 *
 */
package jp.co.shift.base.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * AutoRequression.Propertiesから値を取得するクラス
 *
 */
public class AutoReqressionProperties {

	private static Logger log = LogManager.getLogger(AutoReqressionProperties.class);

	private static AutoReqressionProperties instantce = null;
	private static Properties properties = null;

	// RabbitMQ設定
	public static final String KEY_RABBITMQ_HOST = "rabbitmq.host";
	public static final String KEY_RABBITMQ_PORT = "rabbitmq.port";
	public static final String KEY_RABBITMQ_USER_NAME = "rabbitmq.user.name";
	public static final String KEY_RABBITMQ_PASSWORD = "rabbitmq.password";
	public static final String KEY_RABBITMQ_VIRTUAL_HOST = "rabbitmq.virtual.host";

	// AWS接続キー
	public static final String KEY_S3_ENDPOINT = "s3.endpoint";
	public static final String KEY_S3_REGION = "s3.region";
	public static final String KEY_S3_ACCESS_KEY = "s3.access.key";
	public static final String KEY_S3_SECRET_KEY = "s3.secret.key";

	/**
	 * コンストラクタ(インスタンスを生成させない)
	 *
	 */
	private AutoReqressionProperties() {
		instantce.loadProperties();
	}

	/**
	 * インスタンスを取得する
	 *
	 * @return インスタンス
	 */
	public static AutoReqressionProperties getInstantce() {
		if (instantce == null) {
			instantce = new AutoReqressionProperties();
		}
		return instantce;
	}

	/**
	 * プロパティファイルを読み込む
	 *
	 */
	private void loadProperties() {
		properties = new Properties();
		try {
			properties.load(ClassLoader.getSystemResourceAsStream("autoRegression.properties"));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * プロパティファイルからキー該当する値を取得する
	 *
	 * @param key キー
	 * @return 値
	 */
	public String getProperty(String key) {
		return properties.getProperty(key);
	}

	/**
	 * プロパティファイルからキー該当する数値値を取得する
	 *
	 * @param key キー
	 * @return 値
	 */
	public Integer getIntProperty(String key) {
		try {
			return Integer.parseInt(getProperty(key));
		} catch (NumberFormatException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}
}

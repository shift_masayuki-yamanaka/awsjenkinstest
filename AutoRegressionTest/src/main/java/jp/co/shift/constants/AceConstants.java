/**
 *
 */
package jp.co.shift.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.shift.base.bean.BaseBean;
import jp.co.shift.bean.ace.ApiBean;

/**
 * ACE用定数ファイル
 *
 */
public class AceConstants {

	public static final String ACE_CONNECT_REST = "REST";
	public static final String ACE_CONNECT_SOAP = "SOAP";

	// ACE捜査対象管理Map
	public static Map<String, ApiBean> ACE_API_MAP;
	// ACE識別子管理Map(識別子区分,識別子項目名)
	public static Map<String, List<String>> ACE_ID_KBN_ITEM_MAP;

	static {
		ACE_API_MAP = new HashMap<String, ApiBean>();
		ACE_API_MAP.put("", new ApiBean(BaseBean.class, BaseBean.class, ACE_CONNECT_REST, true, true, "", "", ""));

		ACE_ID_KBN_ITEM_MAP = new HashMap<String, List<String>>();
		ACE_ID_KBN_ITEM_MAP.put("", Arrays.asList("", "", ""));
	}
}

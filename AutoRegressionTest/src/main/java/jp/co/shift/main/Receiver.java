/**
 *
 */
package jp.co.shift.main;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import jp.co.shift.base.util.AutoReqressionProperties;

/**
 * RabbitMQのキュー情報を受信し、各コントローラを実行する
 *
 */
public class Receiver extends Thread {

	private static Logger log = LogManager.getLogger(Receiver.class);

	/**
	 * 起動処理
	 *
	 * @param args
	 */
	public static void maini(String[] args) {
		Receiver receiver = new Receiver();
		receiver.execute();
	}

	public void execute() {
		try {
			Connection connection = getRabbietMqConnection();

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} catch (TimeoutException e) {
			log.error(e.getMessage(), e);
		}

	}

	public void run() {

	}

	/**
	 * RabbietMQのコネクションを生成する
	 *
	 * @return コネクション
	 * @throws IOException
	 * @throws TimeoutException
	 */
	private Connection getRabbietMqConnection() throws IOException, TimeoutException {
		AutoReqressionProperties properties = AutoReqressionProperties.getInstantce();
		String host = properties.getProperty(AutoReqressionProperties.KEY_RABBITMQ_HOST);
		int port = properties.getIntProperty(AutoReqressionProperties.KEY_RABBITMQ_PORT);
		String userName = properties.getProperty(AutoReqressionProperties.KEY_RABBITMQ_USER_NAME);
		String password = properties.getProperty(AutoReqressionProperties.KEY_RABBITMQ_PASSWORD);
		String virtualHost = properties.getProperty(AutoReqressionProperties.KEY_RABBITMQ_VIRTUAL_HOST);

		log.debug("RabbitMQ Setting Info. host:{} port:{} userName:{} password:{} virtualHost:{}", host, port, userName,
				password, virtualHost);

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		factory.setPort(port);
		factory.setUsername(userName);
		factory.setPassword(password);
		factory.setVirtualHost(virtualHost);

		return factory.newConnection();
	}

}

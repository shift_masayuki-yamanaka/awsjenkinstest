/**
 *
 */
package jp.co.shift;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.shift.base.bean.SystemInputInfoBean;
import jp.co.shift.controller.BaseController;
import jp.co.shift.test.SampleBean;

public class ToolTest {

	private static Logger log = LogManager.getLogger(BaseController.class);

	private static ObjectMapper mapper = new ObjectMapper();

	//@Test
	public void jsonTest() throws JsonProcessingException {


		try {
			SystemInputInfoBean bean =  mapper.readValue(getJsonString(), SystemInputInfoBean.class);
			log.debug("systemId:[{}]", bean.getSystemId());
			log.debug("opeDate:[{}]", bean.getOpeDate());
			log.debug("opeContents:[{}]", bean.getOpeContents());
			log.debug("authInfo:[{}]", bean.getAuthInfo());
			log.debug("request:[{}]", bean.getRequest());
			log.debug("response:[{}]", bean.getResponse());
			log.debug("fileInfo:[{}]", bean.getFileInfo());


		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	private String getJsonString() throws JsonProcessingException {
		SystemInputInfoBean bean = new SystemInputInfoBean();

		bean.setSystemId("ACE");
		bean.setOpeContents("qtCheck");
		bean.setOpeDate(new Date());
		bean.setRequest(mapper.writeValueAsString(setData()));
		bean.setResponse(mapper.writeValueAsString(setData()));

		return mapper.writeValueAsString(bean);
	}

	private SampleBean setData() {
		SampleBean bean = new SampleBean();

		bean.setStr1("abcde");
		bean.setStr2("あいうえお");

		bean.setInt1(-12345);
		bean.setInt2(12345);

		bean.setBd1(new BigDecimal(-12345.54321));
		bean.setBd2(new BigDecimal(12345.54321));

		bean.setBool1(true);
		bean.setBool2(false);

		return bean;
	}
}

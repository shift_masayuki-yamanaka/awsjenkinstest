package jp.co.shift.base.comparison;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jp.co.shift.test.SampleBean;
import jp.co.shift.test.SampleListBean;

public class BeanComparisonTest {

	//@Test
	public void test() {
		SampleListBean srcBean = setData(1);
		SampleListBean destBean = setData(2);


		try {
			(new BeanComparison()).comparision(srcBean, destBean);
		} catch (Exception e) {

		}



	}

	private SampleBean setData() {
		SampleBean bean = new SampleBean();

		bean.setStr1("abcde");
		bean.setStr2("あいうえお");

		bean.setInt1(-12345);
		bean.setInt2(12345);

		bean.setBd1(new BigDecimal(-12345.54321));
		bean.setBd2(new BigDecimal(12345.54321));

		bean.setBool1(true);
		bean.setBool2(false);

		return bean;
	}

	private SampleListBean setData(int num) {
		SampleListBean listBean = new SampleListBean();

		List<SampleBean> a = new ArrayList<SampleBean>();
		for (int i = 0 ; i < num ; i++) {
			a.add(setData());
		}

		listBean.setStr1("abcde");
		listBean.setStr2("あいうえお");

		listBean.setInt1(-12345);
		listBean.setInt2(12345);

		listBean.setBd1(new BigDecimal(-12345.54321));
		listBean.setBd2(new BigDecimal(12345.54321));

		listBean.setBool1(true);
		listBean.setBool2(false);

		return listBean;
	}

}

/**
 *
 */
package jp.co.shift.test;

import java.math.BigDecimal;

import jp.co.shift.base.annotation.ExclusionItem;
import jp.co.shift.base.annotation.KeyItem;
import jp.co.shift.base.bean.BaseBean;

public class SampleBean extends BaseBean {

	@KeyItem()
	private String str1;
	private String str2;

	private int int1;
	private int int2;

	@ExclusionItem
	private BigDecimal bd1;
	private BigDecimal bd2;

	private boolean bool1;
	private boolean bool2;

	public String getStr1() {
		return str1;
	}
	public void setStr1(String str1) {
		this.str1 = str1;
	}
	public String getStr2() {
		return str2;
	}
	public void setStr2(String str2) {
		this.str2 = str2;
	}
	public int getInt1() {
		return int1;
	}
	public void setInt1(int int1) {
		this.int1 = int1;
	}
	public int getInt2() {
		return int2;
	}
	public void setInt2(int int2) {
		this.int2 = int2;
	}
	public BigDecimal getBd1() {
		return bd1;
	}
	public void setBd1(BigDecimal bd1) {
		this.bd1 = bd1;
	}
	public BigDecimal getBd2() {
		return bd2;
	}
	public void setBd2(BigDecimal bd2) {
		this.bd2 = bd2;
	}
	public boolean isBool1() {
		return bool1;
	}
	public void setBool1(boolean bool1) {
		this.bool1 = bool1;
	}
	public boolean isBool2() {
		return bool2;
	}
	public void setBool2(boolean bool2) {
		this.bool2 = bool2;
	}
}
